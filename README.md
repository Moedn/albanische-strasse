# Albanische Straße

Ein Würfelspiel, in dem mehr Emotion steckt, als der erste Blick vermuten lässt.
Funktioniert mit 3…5 Bier + X intus noch besser.

**Metadaten:**

- 3…5 Personen
- 3 Würfel pro Person (=9…15)
- 13 Hölzchen (immer, ist auf die Wurfkombos abgestimmt)

Die Regeln sind direkt hier im Repo gerendert einsehbar (siehe [Regeln.md](Regeln.md)); als PDF-Export auch im aktuellen Release ([v1.0](https://gitlab.com/Moedn/albanische-strasse/-/tags/1.0)).

Natürlich darf man neue Regeln einführen :) Falls Ihr neue Spielarten und interessante Regeln entdeckt, lasst es mich gern wissen, indem Ihr einfach ein [Issue](https://gitlab.com/Moedn/albanische-strasse/-/issues/new?issue%5Bmilestone_id%5D=) hier im Repo eröffnet.

Na denn mal viel Spaß :)
