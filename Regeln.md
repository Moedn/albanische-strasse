---
title: Albanische Straße
...

**Metadaten:**

- 3…5 Personen
- 3 Würfel pro Person (=9…15)
- 13 Hölzchen (immer, ist auf die Wurfkombos abgestimmt)

**Hintergrundgeschichte:**

- Henry fragen bzw. seine Aufnahme hören

# Grundgedanke

- Jeder würfelt 1…3-Mal der Reihe nach; wenn alle gewürfelt haben, wird ausgewertet. Der Verlierer bekommt Hölzchen.
- **Ziel:** Hölzchen vermeiden bzw. loswerden. Wer alle Hölzchen hat, verliert.
- Verlierer bekommt einen Minuspunkt, Gewinner einen Pluspunkt.
- Alternativ kann man sich ein cooleres System erdenken.

# Wurfkombos

Der Überlegenheit absteigend sortiert.

| Kombo | Zahlen | Effekt |
| --- | --- | --- |
| Einserpasch | 111 | sofort gewonnen |
| Fuchs | 11n | n Hölzchen |
| Pasch | nnn | 3 Hölzchen |
| Straße | 654, 543, 432, 321 | 2 Hölzchen |
| Albanische Straße (hat Löcher) | 642, 531 | 2 Hölzchen |
| Lusche (Zahlen werden absteigend geordnet) | bspw. 642 | 1 Hölzchen |

\newpage

# Regeln

## Würfeln

- Verlierer der Vorrunde fängt an zu Würfeln 
- max. 3 Würfe pro Spieler; wenn der erste Werfer weniger nutzt, reduziert er das Maximum für alle (bspw. auf 2, wenn er nur zweimal wirft)
- Fällt ein Würfel vom Tisch, wird nochmal geworfen; ansonsten wirft man nie alle Würfel, min. einer muss liegen bleiben (blocken).
- Was liegt, das liegt. Einmal rausgelegte/geblockte Würfel dürfen nicht wieder angefasst werden.

## Auswerten

- Die stärkste Kombo gewinnt.
- Der Verlierer bekommt die Hölzchen vom Stapel (1. Runde) / vom Gewinner (2. Runde).

## 1. Runde, 2. Runde

- In der 1. Runde werden die Hölzchen vom Stapel verteilt.
- Sobald der Stapel leer ist, beginnt die 2. Runde. Man spielt sich die Hölzchen gegenseitig zu.

## Einserpasch

- Würfelt jemand einen Einserpasch, hat er sofort gewonnen.
- Dessen Hölzchen werden aus dem Spiel genommen.
- Für den Rest geht das Spiel normal weiter.

# Rock'n'Roll

Macht berauscht noch mehr Spaß. Dann spielen die Menschen mehr nach Intuition, anstatt Wahrscheinlichkeitsrechnungen anzustrengen.
